// ======================================================================== //
// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "gdt/math/vec.h"
#include "optix7.h"

namespace osc {
using namespace gdt;

struct TriangleMeshSBTData {
  vec3f  color;
  vec3f* vertex;
  vec3i* index;
};

struct VolumeSBTData {
  enum Type {
    TYPE_UINT8,
    TYPE_UINT16,
    TYPE_UINT32,
    TYPE_INT8,
    TYPE_INT16,
    TYPE_INT32,
    TYPE_FLOAT,
    TYPE_DOUBLE
  } type;
  vec3i dims;
  vec3f rcpSpac;
  vec3f rcpDims;
  vec3f color;
  // texture
  cudaTextureObject_t dataTex;
  void*               dataPtr; // sanity check with random access mode
};

struct HitGroupUserData {
  TriangleMeshSBTData mesh;
  VolumeSBTData       volume;
};

struct LaunchParams {
  struct {
    uint32_t* colorBuffer;
    vec2i     size;
  } frame;
  struct {
    vec3f position;
    vec3f direction;
    vec3f horizontal;
    vec3f vertical;
  } camera;
  OptixTraversableHandle traversable = 0;
};

} // namespace osc
