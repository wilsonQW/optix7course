// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "SampleRenderer.h"

/*! \namespace osc - Optix Siggraph Course */
namespace osc {

/*! SBT record for a raygen program */
struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) RaygenRecord
{
  __align__(
    OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE]{};
  // just a dummy value - later examples will use more interesting
  // data here
  void* data{};
};

/*! SBT record for a miss program */
struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) MissRecord
{
  __align__(
    OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE]{};
  // just a dummy value - later examples will use more interesting
  // data here
  void* data{};
};

/*! SBT record for a hitgroup program */
struct __align__(OPTIX_SBT_RECORD_ALIGNMENT) HitgroupRecord
{
  __align__(
    OPTIX_SBT_RECORD_ALIGNMENT) char header[OPTIX_SBT_RECORD_HEADER_SIZE]{};
  HitGroupUserData                   data;
};

/*! constructs the shader binding table */
void
SampleRenderer::buildSBT()
{
  // ------------------------------------------------------------------
  // build raygen records
  // ------------------------------------------------------------------
  std::vector<RaygenRecord> raygenRecords;
  for (auto&& rg : raygenPGs) {
    RaygenRecord rec = {};
    OPTIX_CHECK(optixSbtRecordPackHeader(rg, &rec));
    rec.data = nullptr; /* for now ... */
    raygenRecords.push_back(rec);
  }
  raygenRecordsBuffer.alloc_and_upload(raygenRecords);
  sbt.raygenRecord = raygenRecordsBuffer.d_pointer();

  // ------------------------------------------------------------------
  // build miss records
  // ------------------------------------------------------------------
  std::vector<MissRecord> missRecords;
  for (auto&& ms : missPGs) {
    MissRecord rec = {};
    OPTIX_CHECK(optixSbtRecordPackHeader(ms, &rec));
    rec.data = nullptr; /* for now ... */
    missRecords.push_back(rec);
  }
  missRecordsBuffer.alloc_and_upload(missRecords);
  sbt.missRecordBase          = missRecordsBuffer.d_pointer();
  sbt.missRecordStrideInBytes = sizeof(MissRecord);
  sbt.missRecordCount         = (int)missRecords.size();

  // ------------------------------------------------------------------
  // build hitgroup records
  // ------------------------------------------------------------------
  std::vector<HitgroupRecord> hitgroupRecords;

  // table entries for all meshes
  for (auto&& m : meshes) {
    HitgroupRecord rec;
    // all meshes use the same code, so all same hit group
    OPTIX_CHECK(optixSbtRecordPackHeader(hitgroupPGs[0], &rec));
    rec.data.mesh.color  = m.color;
    rec.data.mesh.vertex = (vec3f*)m.vertexBuffer.d_pointer();
    rec.data.mesh.index  = (vec3i*)m.indexBuffer.d_pointer();
    hitgroupRecords.push_back(rec);
  }

  // table entries for aabbs
  for (auto&& v : volumes) {
    HitgroupRecord rec;
    // all meshes use the same code, so all same hit group
    OPTIX_CHECK(optixSbtRecordPackHeader(hitgroupPGs[1], &rec));
    rec.data.volume.type    = v.type;
    rec.data.volume.dims    = v.dims;
    rec.data.volume.color   = v.color;
    rec.data.volume.dataTex = v.dataTex;
    rec.data.volume.dataPtr = (void*)v.dataBuf.d_pointer();
    hitgroupRecords.push_back(rec);
  }

  hitgroupRecordsBuffer.alloc_and_upload(hitgroupRecords);
  sbt.hitgroupRecordBase          = hitgroupRecordsBuffer.d_pointer();
  sbt.hitgroupRecordStrideInBytes = sizeof(HitgroupRecord);
  sbt.hitgroupRecordCount         = (int)hitgroupRecords.size();
}

} // namespace osc
