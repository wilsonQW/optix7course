// ======================================================================== //
// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "SampleRenderer.h"

// this include may only appear in a single source file:
#include <optix_function_table_definition.h>

#include <fstream>

template<size_t Size>
inline void
swapBytes(void* data)
{
  char* p = reinterpret_cast<char*>(data);
  char* q = p + Size - 1;
  while (p < q)
    std::swap(*(p++), *(q--));
}

template<>
inline void
swapBytes<1>(void*)
{
}

template<>
inline void
swapBytes<2>(void* data)
{
  char* p = reinterpret_cast<char*>(data);
  std::swap(p[0], p[1]);
}

template<>
inline void
swapBytes<4>(void* data)
{
  char* p = reinterpret_cast<char*>(data);
  std::swap(p[0], p[3]);
  std::swap(p[1], p[2]);
}

template<>
inline void
swapBytes<8>(void* data)
{
  char* p = reinterpret_cast<char*>(data);
  std::swap(p[0], p[7]);
  std::swap(p[1], p[6]);
  std::swap(p[2], p[5]);
  std::swap(p[3], p[4]);
}

template<typename T>
inline void
swapBytes(T* data)
{
  swapBytes<sizeof(T)>(reinterpret_cast<void*>(data));
}

void
reverseByteOrder(char* data, size_t elemCount, size_t elemSize)
{
  switch (elemSize) {
  case 1: break;
  case 2:
    for (size_t i = 0; i < elemCount; ++i)
      swapBytes<2>(&data[i * elemSize]);
    break;
  case 4:
    for (size_t i = 0; i < elemCount; ++i)
      swapBytes<4>(&data[i * elemSize]);
    break;
  case 8:
    for (size_t i = 0; i < elemCount; ++i)
      swapBytes<8>(&data[i * elemSize]);
    break;
  default: assert(false);
  }
}

/*! \namespace osc - Optix Siggraph Course */
namespace osc {

extern "C" char embedded_ptx_code[];

//! add aligned cube with front-lower-left corner and size
void
TriangleMesh::addCube(const vec3f& center, const vec3f& size)
{
  PING;
  affine3f xfm;
  xfm.p    = center - 0.5f * size;
  xfm.l.vx = vec3f(size.x, 0.f, 0.f);
  xfm.l.vy = vec3f(0.f, size.y, 0.f);
  xfm.l.vz = vec3f(0.f, 0.f, size.z);
  addUnitCube(xfm);
}

/*! add a unit cube (subject to given xfm matrix) to the current
    triangleMesh */
void
TriangleMesh::addUnitCube(const affine3f& xfm)
{
  int firstVertexID = (int)vertex.size();
  vertex.push_back(xfmPoint(xfm, vec3f(0.f, 0.f, 0.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(1.f, 0.f, 0.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(0.f, 1.f, 0.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(1.f, 1.f, 0.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(0.f, 0.f, 1.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(1.f, 0.f, 1.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(0.f, 1.f, 1.f)));
  vertex.push_back(xfmPoint(xfm, vec3f(1.f, 1.f, 1.f)));

  int indices[] = { 0, 1, 3, 2, 3, 0, 5, 7, 6, 5, 6, 4, 0, 4, 5, 0, 5, 1,
                    2, 3, 7, 2, 7, 6, 1, 5, 7, 1, 7, 3, 4, 0, 2, 4, 2, 6 };
  for (int i = 0; i < 12; i++)
    index.push_back(firstVertexID + vec3i(indices[3 * i + 0],
                                          indices[3 * i + 1],
                                          indices[3 * i + 2]));
}

/*! load the data from disk as raw format */
void
VolumeData::addVolume(const std::string& fileName,
                      // loading parameters
                      Type   dataVoxelType,
                      vec3i  dataDimensions,
                      vec3f  dataSpacing,
                      size_t dataOffset,
                      bool   dataIsBigEndian)
{
  using SBT = VolumeSBTData;

  dims    = dataDimensions;
  type    = dataVoxelType;
  rcpSpac = 1.f / dataSpacing;
  rcpDims = vec3f(reduce_min(1.f / vec3f(dims)));

  auto* vAABB = (vec3f*)&aabb;
  vAABB[0]    = vec3f(0.f);
  vAABB[1]    = vec3f(dims);

  assert(dims.x > 0 && dims.y > 0 && dims.z > 0);

  size_t elemCount = voxelCount();
  size_t elemSize  = voxelSizeInBytes();
  size_t dataSize  = elemCount * elemSize;

  std::ifstream ifs(fileName, std::ios::in | std::ios::binary);
  if (ifs.fail()) { // cannot open the file
    throw std::runtime_error("Cannot open the file");
  }

  ifs.seekg(0, std::ios::end);
  size_t fileSize = ifs.tellg();
  if (fileSize < dataOffset + dataSize) { // file size does not match data size
    throw std::runtime_error("File size does not match data size");
  }
  ifs.seekg(dataOffset, std::ios::beg);

  try {
    dataRaw.reset(new char[dataSize]);
  }
  catch (std::bad_alloc&) { // memory allocation failed
    throw std::runtime_error("Cannot allocate memory for the data");
  }

  // read data
  ifs.read(dataRaw.get(), dataSize);
  if (ifs.fail()) { // reading data failed
    throw std::runtime_error("Cannot read the file");
  }

  const bool reverse = (dataIsBigEndian && elemSize > 1);

  // reverse byte-order if necessary
  if (reverse) {
    reverseByteOrder(dataRaw.get(), elemCount, elemSize);
  }

  ifs.close();
}

/*! access voxel size in bytes */
size_t
VolumeData::voxelCount() const
{
  return (size_t)dims.x * dims.y * dims.z;
}

/*! access voxel count */
size_t
VolumeData::voxelSizeInBytes() const
{
  // clang-format off
    return 
		(type == Type::TYPE_UINT8 || type == Type::TYPE_INT8) ?
        sizeof(uint8_t) :
        (type == Type::TYPE_UINT16 || type == Type::TYPE_INT16) ?
        sizeof(uint16_t) :
        (type == Type::TYPE_UINT32 || type == Type::TYPE_INT32 || type == Type::TYPE_FLOAT) ?
        sizeof(uint32_t) :
        sizeof(double);
  // clang-format on
}

/*! compute 3x4 transformation matrix */
void
VolumeData::transform(float transform[12]) const
{
  vec3f finalScale = scale * rcpSpac * rcpDims;
  transform[0]     = rotate[0] * finalScale.x;
  transform[1]     = rotate[1];
  transform[2]     = rotate[2];
  transform[3]     = center.x - finalScale.x * dims.x * 0.5f;
  transform[4]     = rotate[3];
  transform[5]     = rotate[4] * finalScale.y;
  transform[6]     = rotate[5];
  transform[7]     = center.y - finalScale.y * dims.y * 0.5f;
  transform[8]     = rotate[6];
  transform[9]     = rotate[7];
  transform[10]    = rotate[8] * finalScale.z;
  transform[11]    = center.z - finalScale.z * dims.z * 0.5f;
}

/*! constructor - performs all setup, including initializing
  optix, creates module, pipeline, programs, SBT, etc. */
SampleRenderer::SampleRenderer(std::vector<TriangleMesh>&& meshes,
                               std::vector<VolumeData>&&   volumes)
  : meshes(std::move(meshes)), volumes(std::move(volumes))
{
  initOptix();

  std::cout << "#osc: creating optix context ..." << std::endl;

  createContext();

  std::cout << "#osc: setting up module ..." << std::endl;

  createModule();

  std::cout << "#osc: creating raygen programs ..." << std::endl;

  createRaygenPrograms();

  std::cout << "#osc: creating miss programs ..." << std::endl;

  createMissPrograms();

  std::cout << "#osc: creating hitgroup programs ..." << std::endl;

  createHitgroupPrograms();

  launchParams.traversable = buildAccel_instances();

  std::cout << "#osc: setting up optix pipeline ..." << std::endl;

  createTextures();

  std::cout << "#osc: setting up optix texture ..." << std::endl;

  createPipeline();

  std::cout << "#osc: building SBT ..." << std::endl;

  buildSBT();

  launchParamsBuffer.alloc(sizeof(launchParams));

  std::cout << "#osc: context, module, pipeline, etc, all set up ..."
            << std::endl;
  std::cout << GDT_TERMINAL_GREEN;
  std::cout << "#osc: Optix 7 Sample fully set up" << std::endl;
  std::cout << GDT_TERMINAL_DEFAULT;
}

/*! helper function that initializes optix and checks for errors */
void
SampleRenderer::initOptix()
{
  std::cout << "#osc: initializing OptiX..." << std::endl;

  // -------------------------------------------------------
  // check for available optix7 capable devices
  // -------------------------------------------------------
  cudaFree(0);
  int numDevices;
  cudaGetDeviceCount(&numDevices);
  if (numDevices == 0)
    throw std::runtime_error("#osc: no CUDA capable devices found!");
  std::cout << "#osc: found " << numDevices << " CUDA devices" << std::endl;

  // -------------------------------------------------------
  // initialize optix
  // -------------------------------------------------------
  OPTIX_CHECK(optixInit());
  std::cout << "#osc: successfully initialized OptiX" << std::endl;
}

static void
context_log_cb(unsigned int level, const char* tag, const char* message, void*)
{
  fprintf(stderr, "[%2d][%12s]: %s\n", (int)level, tag, message);
}

/*! creates and configures a optix device context (in this simple
  example, only for the primary GPU device) */
void
SampleRenderer::createContext()
{
  // for this sample, do everything on one device
  const int deviceID = 0;
  CUDA_CHECK(SetDevice(deviceID));
  CUDA_CHECK(StreamCreate(&stream));

  cudaGetDeviceProperties(&deviceProps, deviceID);
  std::cout << "#osc: running on device: " << deviceProps.name << std::endl;

  CUresult cuRes = cuCtxGetCurrent(&cudaContext);
  if (cuRes != CUDA_SUCCESS)
    fprintf(stderr, "Error querying current context: error code %d\n", cuRes);

  OPTIX_CHECK(optixDeviceContextCreate(cudaContext, 0, &optixContext));
  OPTIX_CHECK(
    optixDeviceContextSetLogCallback(optixContext, context_log_cb, nullptr, 4));
}

/*! creates the module that contains all the programs we are going
  to use. in this simple example, we use a single module from a
  single .cu file, using a single embedded ptx string */
void
SampleRenderer::createModule()
{
  moduleCompileOptions.maxRegisterCount = 100;
  moduleCompileOptions.optLevel         = OPTIX_COMPILE_OPTIMIZATION_LEVEL_0;
  moduleCompileOptions.debugLevel       = OPTIX_COMPILE_DEBUG_LEVEL_LINEINFO;

  pipelineCompileOptions = {};
  pipelineCompileOptions.traversableGraphFlags =
    OPTIX_TRAVERSABLE_GRAPH_FLAG_ALLOW_ANY;
  pipelineCompileOptions.usesMotionBlur     = false;
  pipelineCompileOptions.numPayloadValues   = 2;
  pipelineCompileOptions.numAttributeValues = 8;
  pipelineCompileOptions.exceptionFlags     = OPTIX_EXCEPTION_FLAG_NONE;
  pipelineCompileOptions.pipelineLaunchParamsVariableName = "optixLaunchParams";

  pipelineLinkOptions.overrideUsesMotionBlur = false;
  pipelineLinkOptions.maxTraceDepth          = 2;

  const std::string ptxCode = embedded_ptx_code;

  char   log[2048];
  size_t sizeof_log = sizeof(log);
  OPTIX_CHECK(optixModuleCreateFromPTX(optixContext,
                                       &moduleCompileOptions,
                                       &pipelineCompileOptions,
                                       ptxCode.c_str(),
                                       ptxCode.size(),
                                       log,
                                       &sizeof_log,
                                       &module));
  if (sizeof_log > 1)
    PRINT(log);
}

/*! assembles the full pipeline of all programs */
void
SampleRenderer::createPipeline()
{
  std::vector<OptixProgramGroup> programGroups;
  for (auto pg : raygenPGs)
    programGroups.push_back(pg);
  for (auto pg : missPGs)
    programGroups.push_back(pg);
  for (auto pg : hitgroupPGs)
    programGroups.push_back(pg);

  char   log[2048];
  size_t sizeof_log = sizeof(log);
  OPTIX_CHECK(optixPipelineCreate(optixContext,
                                  &pipelineCompileOptions,
                                  &pipelineLinkOptions,
                                  programGroups.data(),
                                  (int)programGroups.size(),
                                  log,
                                  &sizeof_log,
                                  &pipeline));
  if (sizeof_log > 1)
    PRINT(log);

  OPTIX_CHECK(
    optixPipelineSetStackSize(/* [in] The pipeline to configure the stack size
                                 for */
                              pipeline,
                              /* [in] The direct stack size requirement for
                                 direct callables invoked from IS or AH. */
                              2 * 1024,
                              /* [in] The direct stack size requirement for
                                 direct
                                 callables invoked from RG, MS, or CH.  */
                              2 * 1024,
                              /* [in] The continuation stack requirement. */
                              2 * 1024,
                              /* [in] The maximum depth of a traversable graph
                                 passed to trace. */
                              3));
  if (sizeof_log > 1)
    PRINT(log);
}

void
SampleRenderer::createTextures() // TODO is this correct ?
{
  for (auto&& volume : volumes) {

    // TODO correct? for multiple types?
    cudaChannelFormatDesc channel_desc = cudaCreateChannelDesc<uint8_t>();
    CUDA_CHECK(Malloc3DArray(
      &volume.dataArr,
      &channel_desc,
      make_cudaExtent(volume.dims.x, volume.dims.y, volume.dims.z)));

    std::cout << "#osc: allocate 3D texture ..." << std::endl;

    cudaMemcpy3DParms param = { 0 };
    param.srcPos            = make_cudaPos(0, 0, 0);
    param.dstPos            = make_cudaPos(0, 0, 0);
    param.srcPtr =
      make_cudaPitchedPtr(volume.dataRaw.get(),
                          volume.dims.x * volume.voxelSizeInBytes(),
                          volume.dims.x,
                          volume.dims.y);
    param.dstArray = volume.dataArr;
    param.extent = make_cudaExtent(volume.dims.x, volume.dims.y, volume.dims.z);
    param.kind   = cudaMemcpyHostToDevice;
    CUDA_CHECK(Memcpy3D(&param));
    std::cout << "#osc: copy 3D texture to device ..." << std::endl;

    cudaResourceDesc res_desc = {};
    res_desc.resType          = cudaResourceTypeArray;
    res_desc.res.array.array  = volume.dataArr;

    cudaTextureDesc tex_desc     = {};
    tex_desc.addressMode[0]      = cudaAddressModeWrap;
    tex_desc.addressMode[1]      = cudaAddressModeWrap;
    tex_desc.addressMode[2]      = cudaAddressModeWrap;
    tex_desc.filterMode          = cudaFilterModeLinear;
    tex_desc.readMode            = cudaReadModeNormalizedFloat;
    tex_desc.normalizedCoords    = 1;
    tex_desc.maxAnisotropy       = 1;
    tex_desc.maxMipmapLevelClamp = 99;
    tex_desc.minMipmapLevelClamp = 0;
    tex_desc.mipmapFilterMode    = cudaFilterModePoint;
    tex_desc.borderColor[0]      = 1.0f;
    tex_desc.sRGB                = 0;

    // Create texture object
    cudaTextureObject_t tex_obj = 0;
    CUDA_CHECK(CreateTextureObject(&tex_obj, &res_desc, &tex_desc, nullptr));
    volume.dataTex = tex_obj;
  }
}

/*! render one frame */
void
SampleRenderer::render()
{
  // sanity check: make sure we launch only after first resize is
  // already done:
  if (launchParams.frame.size.x == 0)
    return;

  launchParamsBuffer.upload(&launchParams, 1);

  OPTIX_CHECK(optixLaunch(/*! pipeline we're launching launch: */
                          pipeline,
                          stream,
                          /*! parameters and SBT */
                          launchParamsBuffer.d_pointer(),
                          launchParamsBuffer.sizeInBytes,
                          &sbt,
                          /*! dimensions of the launch: */
                          launchParams.frame.size.x,
                          launchParams.frame.size.y,
                          1));
  // sync - make sure the frame is rendered before we download and
  // display (obviously, for a high-performance application you
  // want to use streams and double-buffering, but for this simple
  // example, this will have to do)
  CUDA_SYNC_CHECK();
}

/*! set camera to render with */
void
SampleRenderer::setCamera(const Camera& camera)
{
  lastSetCamera                 = camera;
  launchParams.camera.position  = camera.from;
  launchParams.camera.direction = normalize(camera.at - camera.from);
  const float cosFovy           = 0.66f;
  const float aspect =
    launchParams.frame.size.x / float(launchParams.frame.size.y);
  launchParams.camera.horizontal =
    cosFovy * aspect *
    normalize(cross(launchParams.camera.direction, camera.up));
  launchParams.camera.vertical =
    cosFovy * normalize(cross(launchParams.camera.horizontal,
                              launchParams.camera.direction));
}

/*! resize frame buffer to given resolution */
void
SampleRenderer::resize(const vec2i& newSize)
{
  // resize our cuda frame buffer
  frameBuffer.resize(newSize.x * newSize.y * sizeof(uint32_t));

  // update the launch parameters that we'll pass to the optix
  // launch:
  launchParams.frame.size        = newSize;
  launchParams.frame.colorBuffer = (uint32_t*)frameBuffer.d_pointer();

  // and re-set the camera, since aspect may have changed
  setCamera(lastSetCamera);
}

/*! download the rendered color buffer */
void
SampleRenderer::downloadPixels(uint32_t h_pixels[])
{
  frameBuffer.download(h_pixels,
                       launchParams.frame.size.x * launchParams.frame.size.y);
}

} // namespace osc
