// ======================================================================== //
// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include <optix_device.h>

#include "LaunchParams.h"

using namespace osc;

namespace osc {

/*! launch parameters in constant memory, filled in by optix upon
    optixLaunch (this gets filled in from the buffer we pass to
    optixLaunch) */
extern "C" __constant__ LaunchParams optixLaunchParams;

// for this simple example, we have a single ray type
enum { SURFACE_RAY_TYPE = 0, RAY_TYPE_COUNT };

static __forceinline__ __device__ void*
                       unpackPointer(uint32_t i0, uint32_t i1)
{
  const auto uptr = static_cast<uint64_t>(i0) << 32U | i1;
  void*      ptr  = reinterpret_cast<void*>(uptr);
  return ptr;
}

static __forceinline__ __device__ void
                       packPointer(void* ptr, uint32_t& i0, uint32_t& i1)
{
  const auto uptr = reinterpret_cast<uint64_t>(ptr);
  i0              = uptr >> 32U;
  i1              = uptr & 0x00000000ffffffff;
}

template<typename T>
static __forceinline__ __device__ T*
                                  getPRD()
{
  const uint32_t u0 = optixGetPayload_0();
  const uint32_t u1 = optixGetPayload_1();
  return reinterpret_cast<T*>(unpackPointer(u0, u1));
}

inline __device__ bool
intersectBox(float&       t0,
             float&       t1,
             const vec3f& ray_ori,
             const vec3f& ray_dir,
             const vec3f& lower,
             const vec3f& upper)
{
  const vec3f t_lo = (lower - ray_ori) / ray_dir;
  const vec3f t_hi = (upper - ray_ori) / ray_dir;

  t0 = max(t0, reduce_max(min(t_lo, t_hi)));
  t1 = min(t1, reduce_min(max(t_lo, t_hi)));

  return t1 > t0;
}

//------------------------------------------------------------------------------
// closest hit and anyhit programs for radiance-type rays.
//
// Note eventually we will have to create one pair of those for each
// ray type and each geometry type we want to render; but this
// simple example doesn't use any actual geometries yet, so we only
// create a single, dummy, set of them (we do have to have at least
// one group of them to set up the SBT)
//------------------------------------------------------------------------------

extern "C" __global__ void
__closesthit__radiance()
{
  const auto* sbtData = (const HitGroupUserData*)optixGetSbtDataPointer();
  const TriangleMeshSBTData& meshData = sbtData->mesh;

  // compute normal:
  const uint32_t primID = optixGetPrimitiveIndex();
  const vec3i    index  = meshData.index[primID];
  const vec3f&   A      = meshData.vertex[index.x];
  const vec3f&   B      = meshData.vertex[index.y];
  const vec3f&   C      = meshData.vertex[index.z];
  const vec3f    Ng     = normalize(cross(B - A, C - A));

  const vec3f rayDir = optixGetWorldRayDirection();
  const float cosDN  = 0.2f + .8f * fabsf(dot(rayDir, Ng));
  vec4f&      prd    = *(vec4f*)getPRD<vec4f>();
  prd                = vec4f(cosDN * meshData.color, 1.f);
}

extern "C" __global__ void
__anyhit__radiance() /*! for this simple example, this will remain empty */
{
//  printf("any hit radiance %i\n", optixGetPrimitiveIndex());
}

//------------------------------------------------------------------------------
// intersection program that computes customized intersections for an AABB
//
// ------------------------------------------------------------------------------

inline __device__ vec4f
                  integrateVolume(const VolumeSBTData& volume,
                                  // background color and alpha
                                  const vec3f& inColor,
                                  const float& inAlpha,
                                  // object space ray direction and origin
                                  const vec3f& ori,
                                  const vec3f& dir,
                                  // ray distances
                                  const float& tMin,
                                  const float& tMax,
                                  // sampling parameter
                                  const float& rate)
{
  const vec3i& dims = volume.dims;
  const float  step = 1.f / rate;

  vec3f color = inColor;
  float alpha = inAlpha;
  float t     = tMin;

  while (t <= tMax) {

#if 1 // sample data value
    const vec3f p = ori + t * dir;
    const auto  v =
      tex3D<float>(volume.dataTex, p.x / dims.x, p.y / dims.y, p.z / dims.z);
#else
    const vec3f p = ori + t * dir;
    const vec3i i = min(vec3i(p), dims - 1);
    const float v =
      ((uint8_t*)volume.dataPtr)[i.x + i.y * dims.x + i.z * dims.x * dims.y] /
      255.f;
#endif
    // classification
    vec3f c = vec3f(1.f - v);
    float a = v * 0.2f;

    // alpha blending
    color += float(1 - alpha) * c * a;
    alpha += float(1 - alpha) * a;

    t += step;
  }

  return vec4f(color, alpha);
}

extern "C" __global__ void
__closesthit__box()
{
#if 1
  const auto* sbtData = (const HitGroupUserData*)optixGetSbtDataPointer();
  const auto& volume  = sbtData->volume;

  vec4f& prd = *(vec4f*)getPRD<vec4f>();

  const vec3f ori(
    __int_as_float(optixGetAttribute_0()), // passing the object space ray
    __int_as_float(optixGetAttribute_1()), // from intersection shader
    __int_as_float(optixGetAttribute_2()));

  const vec3f dir(
    __int_as_float(optixGetAttribute_3()), // passing the object space ray
    __int_as_float(optixGetAttribute_4()), // from intersection shader
    __int_as_float(optixGetAttribute_5()));

  const float t0 = __int_as_float(optixGetAttribute_6());
  const float t1 = __int_as_float(optixGetAttribute_7());

  prd = integrateVolume(
    volume, vec3f(prd.x, prd.y, prd.z), prd.w, ori, dir, t0, t1, 100);
#endif
}

extern "C" __global__ void
__anyhit__box() /*! for this simple example, this will remain empty */
{
//  printf("any hit box %i\n", optixGetPrimitiveIndex());
}

extern "C" __global__ void
__intersection__box()
{
  const auto* sbtData = (const HitGroupUserData*)optixGetSbtDataPointer();
  const auto& volume  = sbtData->volume;

  const vec3f ori = optixGetObjectRayOrigin();
  const vec3f dir = optixGetObjectRayDirection();

  float t0 = optixGetRayTmin();
  float t1 = optixGetRayTmax();

  if (intersectBox(t0, t1, ori, dir, vec3f(0.f), vec3f(volume.dims))) {
    // printf("%i\n", optixGetPrimitiveIndex());
    optixReportIntersection(t0,
                            0, /* attributes */
                            __float_as_int(ori.x),
                            __float_as_int(ori.y),
                            __float_as_int(ori.z), //
                            __float_as_int(dir.x),
                            __float_as_int(dir.y),
                            __float_as_int(dir.z), //
                            __float_as_int(t0),
                            __float_as_int(t1));
  }
}

//------------------------------------------------------------------------------
// miss program that gets called for any ray that did not have a
// valid intersection
//
// as with the anyhit/closest hit programs, in this example we only
// need to have _some_ dummy function to set up a valid SBT
// ------------------------------------------------------------------------------

extern "C" __global__ void
__miss__radiance()
{
  vec3f& prd = *(vec3f*)getPRD<vec3f>();
  // set to constant white as background color
  prd = vec3f(1.f);
}

//------------------------------------------------------------------------------
// ray gen program - the actual rendering happens in here
//------------------------------------------------------------------------------
extern "C" __global__ void
__raygen__renderFrame()
{
  // compute a test pattern based on pixel ID
  const int ix = optixGetLaunchIndex().x;
  const int iy = optixGetLaunchIndex().y;

  const auto& camera = optixLaunchParams.camera;

  // our per-ray data for this example. what we initialize it to
  // won't matter, since this value will be overwritten by either
  // the miss or hit program, anyway
  vec4f pixelColorPRD = vec4f(0.f);

  // the values we store the PRD pointer in:
  uint32_t u0, u1;
  packPointer(&pixelColorPRD, u0, u1);

  // normalized screen plane position, in [0,1]^2
  const vec2f screen(vec2f((float)ix + .5f, (float)iy + .5f) /
                     vec2f(optixLaunchParams.frame.size));

  // generate ray direction
  vec3f rayDir =
    normalize(camera.direction + (screen.x - 0.5f) * camera.horizontal +
              (screen.y - 0.5f) * camera.vertical);

  optixTrace(optixLaunchParams.traversable,
             camera.position,
             rayDir,
             0.f,   // tmin
             1e20f, // tmax
             0.0f,  // rayTime
             OptixVisibilityMask(255),
             // OPTIX_RAY_FLAG_NONE,
             OPTIX_RAY_FLAG_DISABLE_ANYHIT,
             SURFACE_RAY_TYPE,              // SBT offset
             RAY_TYPE_COUNT,                // SBT stride
             SURFACE_RAY_TYPE,              // missSBTIndex
             u0,
             u1);

  const uint32_t r(255.99f * pixelColorPRD.x);
  const uint32_t g(255.99f * pixelColorPRD.y);
  const uint32_t b(255.99f * pixelColorPRD.z);
  const uint32_t a(255.99f * pixelColorPRD.w);

  // convert to 32-bit rgba value (we explicitly set alpha to 0xff
  // to make stb_image_write happy ...
  const uint32_t rgba = (r << 0U) | (g << 8U) | (b << 16U) | (a << 24U);

  // pixel index
  const uint32_t fbIndex = ix + iy * optixLaunchParams.frame.size.x;

  // and write to frame buffer ...
  optixLaunchParams.frame.colorBuffer[fbIndex] = rgba;
}

} // namespace osc
