// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "SampleRenderer.h"

#include <cstdio>
#include <cstring>

/*! \namespace osc - Optix Siggraph Course */
namespace osc {

OptixTraversableHandle
SampleRenderer::buildAccel_exec(std::vector<OptixBuildInput>& input)
{

  OptixTraversableHandle asHandle{ 0 };

  // ==================================================================
  // BLAS setup
  // ==================================================================

  OptixAccelBuildOptions accelOptions = {};
  accelOptions.buildFlags =
    OPTIX_BUILD_FLAG_NONE | OPTIX_BUILD_FLAG_ALLOW_COMPACTION;
  accelOptions.operation = OPTIX_BUILD_OPERATION_BUILD;

  OptixAccelBufferSizes blasBufferSizes;
  OPTIX_CHECK(
    optixAccelComputeMemoryUsage(optixContext,
                                 &accelOptions,
                                 input.data(),
                                 (int)input.size(), // num_build_inputs
                                 &blasBufferSizes));

  // ==================================================================
  // prepare compaction
  // ==================================================================

  CUDABuffer compactedSizeBuffer;
  compactedSizeBuffer.alloc(sizeof(uint64_t));

  OptixAccelEmitDesc emitDesc;
  emitDesc.type   = OPTIX_PROPERTY_TYPE_COMPACTED_SIZE;
  emitDesc.result = compactedSizeBuffer.d_pointer();

  // ==================================================================
  // execute build (main stage)
  // ==================================================================

  CUDABuffer tempBuffer;
  tempBuffer.alloc(blasBufferSizes.tempSizeInBytes);

  CUDABuffer outputBuffer;
  outputBuffer.alloc(blasBufferSizes.outputSizeInBytes);

  OPTIX_CHECK(optixAccelBuild(optixContext,
                              0 /* stream */,
                              &accelOptions,
                              input.data(),
                              (int)input.size(),
                              tempBuffer.d_pointer(),
                              tempBuffer.sizeInBytes,
                              outputBuffer.d_pointer(),
                              outputBuffer.sizeInBytes,
                              &asHandle,
                              &emitDesc,
                              1));
  CUDA_SYNC_CHECK();

  // ==================================================================
  // perform compaction
  // ==================================================================

  uint64_t compactedSize;
  compactedSizeBuffer.download(&compactedSize, 1);

  asBuffers.emplace_back();
  auto& asBuffer = asBuffers.back();
  asBuffer.alloc(compactedSize);
  OPTIX_CHECK(optixAccelCompact(optixContext,
                                0 /*stream:*/,
                                asHandle,
                                asBuffer.d_pointer(),
                                asBuffer.sizeInBytes,
                                &asHandle));
  CUDA_SYNC_CHECK();

  // ==================================================================
  // aaaaaand .... clean up
  // ==================================================================

  outputBuffer.free(); // << the UNcompacted, temporary output buffer
  tempBuffer.free();
  compactedSizeBuffer.free();

  return asHandle;
}

OptixTraversableHandle
SampleRenderer::buildAccel_triangles()
{
  // ==================================================================
  // triangle inputs
  // ==================================================================

  std::vector<OptixBuildInput> triangleInputs(meshes.size());
  std::vector<CUdeviceptr>     d_vertices(meshes.size());
  std::vector<CUdeviceptr>     d_indices(meshes.size());
  std::vector<uint32_t>        f_triangles(meshes.size());

  for (size_t meshID = 0; meshID < meshes.size(); meshID++) {

    // upload the model to the device: the builder
    TriangleMesh& mesh = meshes[meshID];
    mesh.vertexBuffer.alloc_and_upload(mesh.vertex);
    mesh.indexBuffer.alloc_and_upload(mesh.index);

    // create local variables, because we need a *pointer* to the
    // device pointers
    d_vertices[meshID]  = mesh.vertexBuffer.d_pointer();
    d_indices[meshID]   = mesh.indexBuffer.d_pointer();
    f_triangles[meshID] = 0;

    OptixBuildInput triangleInput            = {};
    triangleInput.type                       = OPTIX_BUILD_INPUT_TYPE_TRIANGLES;
    triangleInput.triangleArray.vertexFormat = OPTIX_VERTEX_FORMAT_FLOAT3;
    triangleInput.triangleArray.vertexStrideInBytes = sizeof(vec3f);
    triangleInput.triangleArray.numVertices         = (int)mesh.vertex.size();
    triangleInput.triangleArray.vertexBuffers       = &d_vertices[meshID];
    triangleInput.triangleArray.indexFormat =
      OPTIX_INDICES_FORMAT_UNSIGNED_INT3;
    triangleInput.triangleArray.indexStrideInBytes = sizeof(vec3i);
    triangleInput.triangleArray.numIndexTriplets   = (int)mesh.index.size();
    triangleInput.triangleArray.indexBuffer        = d_indices[meshID];

    // in this example we have one SBT entry, and no per-primitive materials:
    triangleInput.triangleArray.flags                = &f_triangles[meshID];
    triangleInput.triangleArray.numSbtRecords        = 1;
    triangleInput.triangleArray.sbtIndexOffsetBuffer = 0;
    triangleInput.triangleArray.sbtIndexOffsetSizeInBytes   = 0;
    triangleInput.triangleArray.sbtIndexOffsetStrideInBytes = 0;

    triangleInputs[meshID] = triangleInput;
  }

  return buildAccel_exec(triangleInputs);
}

OptixTraversableHandle
SampleRenderer::buildAccel_a_volume(VolumeData& volume)
{
  // ==================================================================
  // aabb inputs
  // ==================================================================

  volume.dataBuf.alloc_and_upload(
    volume.dataRaw.get(), volume.voxelCount() * volume.voxelSizeInBytes());
  volume.aabbBuffer.alloc_and_upload(&volume.aabb, 1);

  CUdeviceptr d_aabb = volume.aabbBuffer.d_pointer();
  uint32_t    f_aabb = 0;

  OptixBuildInput volumeInput       = {}; // use one AABB input
  volumeInput.type                  = OPTIX_BUILD_INPUT_TYPE_CUSTOM_PRIMITIVES;
  volumeInput.aabbArray.aabbBuffers = &d_aabb;
  volumeInput.aabbArray.numPrimitives               = 1;
  volumeInput.aabbArray.strideInBytes               = 0;
  volumeInput.aabbArray.primitiveIndexOffset        = 0;
  volumeInput.aabbArray.flags                       = &f_aabb;
  volumeInput.aabbArray.numSbtRecords               = 1;
  volumeInput.aabbArray.sbtIndexOffsetBuffer        = 0;
  volumeInput.aabbArray.sbtIndexOffsetSizeInBytes   = 0;
  volumeInput.aabbArray.sbtIndexOffsetStrideInBytes = 0;

  std::vector<OptixBuildInput> inputs = { volumeInput };
  return buildAccel_exec(inputs);
}

OptixTraversableHandle
SampleRenderer::buildAccel_instances()
{
  auto tTraversable = buildAccel_triangles();

  std::vector<OptixTraversableHandle> vTraversable(volumes.size());
  for (int i = 0; i < (int)volumes.size(); ++i) {
    vTraversable[i] = buildAccel_a_volume(volumes[i]);
  }

  // ==================================================================
  // Create Instances
  // ==================================================================

  float         transform[12] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0 };
  OptixInstance triangleIAS   = {};
  memcpy(triangleIAS.transform, transform, sizeof(float) * 12);
  triangleIAS.instanceId        = 0;
  triangleIAS.visibilityMask    = 255;
  triangleIAS.sbtOffset         = 0;
  triangleIAS.flags             = OPTIX_INSTANCE_FLAG_NONE;
  triangleIAS.traversableHandle = tTraversable;
  instances.push_back(triangleIAS);

  for (size_t volumeID = 0; volumeID < volumes.size(); ++volumeID) {
    OptixInstance instance = {};
    volumes[volumeID].transform(instance.transform);
    instance.instanceId        = 1 + volumeID;
    instance.visibilityMask    = 255;
    instance.sbtOffset         = meshes.size() + volumeID;
    instance.flags             = OPTIX_INSTANCE_FLAG_NONE;
    instance.traversableHandle = vTraversable[volumeID];
    instances.push_back(instance);
  }

  instancesBuffer.alloc_and_upload(instances);

  // ==================================================================
  // Create Inputs
  // ==================================================================

  OptixBuildInput iasInput            = {};
  iasInput.type                       = OPTIX_BUILD_INPUT_TYPE_INSTANCES;
  iasInput.instanceArray.instances    = instancesBuffer.d_pointer();
  iasInput.instanceArray.numInstances = instances.size();

  std::vector<OptixBuildInput> inputs = { iasInput };
  return buildAccel_exec(inputs);
}

} // namespace osc
