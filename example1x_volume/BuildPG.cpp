// Copyright 2018-2019 Ingo Wald                                            //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "SampleRenderer.h"

#include <cstring>

/*! \namespace osc - Optix Siggraph Course */
namespace osc {

/*! does all setup for the raygen program(s) we are going to use */
void
SampleRenderer::createRaygenPrograms()
{
  // we do a single ray gen program in this example:
  raygenPGs.resize(1);

  OptixProgramGroupOptions pgOptions = {};
  OptixProgramGroupDesc    pgDesc    = {};
  pgDesc.kind                        = OPTIX_PROGRAM_GROUP_KIND_RAYGEN;
  pgDesc.raygen.module               = module;
  pgDesc.raygen.entryFunctionName    = "__raygen__renderFrame";

  // OptixProgramGroup raypg;
  char   log[2048];
  size_t sizeof_log = sizeof(log);
  OPTIX_CHECK(optixProgramGroupCreate(
    optixContext, &pgDesc, 1, &pgOptions, log, &sizeof_log, &raygenPGs[0]));
  if (sizeof_log > 1)
    PRINT(log);
}

/*! does all setup for the miss program(s) we are going to use */
void
SampleRenderer::createMissPrograms()
{
  // we do a single ray gen program in this example:
  missPGs.resize(1);

  OptixProgramGroupOptions pgOptions = {};
  OptixProgramGroupDesc    pgDesc    = {};
  pgDesc.kind                        = OPTIX_PROGRAM_GROUP_KIND_MISS;
  pgDesc.miss.module                 = module;
  pgDesc.miss.entryFunctionName      = "__miss__radiance";

  // OptixProgramGroup raypg;
  char   log[2048];
  size_t sizeof_log = sizeof(log);
  OPTIX_CHECK(optixProgramGroupCreate(
    optixContext, &pgDesc, 1, &pgOptions, log, &sizeof_log, &missPGs[0]));
  if (sizeof_log > 1)
    PRINT(log);
}

/*! does all setup for the hitgroup program(s) we are going to use */
void
SampleRenderer::createHitgroupPrograms()
{
  // for this simple example, we set up a single hit group
  hitgroupPGs.resize(2);

  OptixProgramGroupOptions pgOptions = {};
  OptixProgramGroupDesc    pgDesc    = {};

  char   log[2048];
  size_t sizeof_log = sizeof(log);

  pgDesc                              = {};
  pgDesc.kind                         = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
  pgDesc.hitgroup.moduleCH            = module;
  pgDesc.hitgroup.entryFunctionNameCH = "__closesthit__radiance";
  pgDesc.hitgroup.moduleAH            = module;
  pgDesc.hitgroup.entryFunctionNameAH = "__anyhit__radiance";

  OPTIX_CHECK(optixProgramGroupCreate(
    optixContext, &pgDesc, 1, &pgOptions, log, &sizeof_log, &hitgroupPGs[0]));

  memset(&pgDesc, 0, sizeof(OptixProgramGroupDesc));
  pgDesc.kind                         = OPTIX_PROGRAM_GROUP_KIND_HITGROUP;
  pgDesc.hitgroup.moduleCH            = module;
  pgDesc.hitgroup.entryFunctionNameCH = "__closesthit__box";
  pgDesc.hitgroup.moduleAH            = module;
  pgDesc.hitgroup.entryFunctionNameAH = "__anyhit__box";
  pgDesc.hitgroup.moduleIS            = module;
  pgDesc.hitgroup.entryFunctionNameIS = "__intersection__box";

  OPTIX_CHECK(optixProgramGroupCreate(
    optixContext, &pgDesc, 1, &pgOptions, log, &sizeof_log, &hitgroupPGs[1]));

  if (sizeof_log > 1)
    PRINT(log);
}

} // namespace osc
